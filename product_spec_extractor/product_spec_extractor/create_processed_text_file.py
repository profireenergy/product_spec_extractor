import os
import glob

class CreateProcessedTextFile:
    def CreateProcessedTextFile(inputPath, outputPath):
        # find the txt file
        countText = 0
        for inputTextFile in glob.glob(os.path.join(inputPath,'*.txt')):
            print(inputTextFile)
            countText += 1

        if (countText > 1):
            print("Too Many Text Files")
            return(-1)

        outputTextFile = inputTextFile.replace("input","output")
        outputTextFile = outputTextFile.replace(".txt"," Processed.txt")

        # create processed text file
        outputTextFileTouch = open(outputTextFile, "w+")

        print(outputTextFile)

        startingLine = False
        startWriting = False

        with open(inputTextFile, "r", encoding="utf-8") as textRead:
            lines = textRead.readlines()

        with open(outputTextFile, "w", encoding="utf-8") as textWrite:
            for line in lines:

                #double if statement to skip the first line
                if (startingLine == True):
                    startWriting = True

                if ("Level_Unique" in line):
                    startingLine = True
        
                # stop writing
                if ("Confidential" in line):
                    startWriting = False
                    startingLine = False

                # only write if startwriting is enabled and if the line is not empty
                if((startWriting) & (line.strip("\n") != "")):
           
           
                     textWrite.write(line)

        return(outputTextFile)


