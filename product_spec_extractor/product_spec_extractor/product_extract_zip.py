import os
import glob
import zipfile
import py7zr

class ProductExtractZip:
    def ProductExtractZip(inputPath, outputPath):

        # find the zip file
        countZip = 0
        for inputProductZipFile in glob.glob(os.path.join(inputPath,'*.zip')):
            print("input Product Zip File: ", inputProductZipFile)
            countZip += 1

        if (countZip > 1):
            print("Too Many Zip Files")
            return(-1)

        outputProductZipFile = inputProductZipFile.replace("input", "output")

        outputZipBasename = os.path.basename(outputProductZipFile)

        with zipfile.ZipFile(inputProductZipFile) as zipRef:
            zipRef.extractall(os.path.join(outputPath, outputZipBasename))



