# Python 3.9 script | product_spec_extractor
# A program to find all the spec files in a product zip folder and extract them into their own zip folders

# imports
# --------------------------------------------------

import os
import time
import datetime
import sys

import tkinter as tk
from tkinter import filedialog, messagebox

import zipfile
import glob
import shutil
import fnmatch

from pathlib import Path

from create_processed_text_file import CreateProcessedTextFile

from product_extract_zip import ProductExtractZip

# variables
# --------------------------------------------------
pythonPath = os.getcwd()

print(pythonPath)

inputPath = os.path.join(pythonPath, '..', '..', 'input' )
outputPath = os.path.join(pythonPath, '..', '..', 'output')

print(inputPath)
print(outputPath)


# pull the file names from the text file and output them to outputTextFile
outputTextFile = CreateProcessedTextFile.CreateProcessedTextFile(inputPath, outputPath)
if (outputTextFile == -1):
    exit(-1)
print("returned output text file: ", outputTextFile)

#outputExtractedFolder = ProductExtractZip.ProductExtractZip(inputPath, outputPath)
#if (outputExtractedFolder == -1):
#    exit(-1)
#print("returned output folder path: ", outputExtractedFolder)






#outputProductZipFile = inputProductZipFile.replace("input", "output")
#outputProductZipFile = outputProductZipFile.replace(".zip", "")

#outputZipBasename = os.path.basename(outputProductZipFile)



#with zipfile.ZipFile(inputProductZipFile, 'r') as zipRef:
#    zipRef.extractall(os.path.join(outputPath, outputZipBasename))


# find files using the text document

# find the zip file
countZip = 0
for inputProductZipFile in glob.glob(os.path.join(inputPath,'*.zip')):
    print("input Product Zip File: ", inputProductZipFile)
    countZip += 1

if (countZip > 1):
    print("Too Many Zip Files")
    exit(-1)

inputProductFolderName = inputProductZipFile.replace(".zip", "")

# product = [Level Unique PN, Eng PN, ECN, Description]
iter_LevelUniquePn = 0
iter_EngPn = 1
iter_Ecn = 2
iter_Description = 3


with open(outputTextFile, 'r', encoding="utf-8") as productList:
    lines = productList.readlines()

    for line in lines:
        print("--------------------------------------------------------------------------------")

        print(line)

        #important for all class methods
        # split 3 times, leaving the description in tact
        levelUniquePnStruct = [0,0]
        listLine = line.split(" ",3)
        print("0: ", listLine[0])
        print("1: ", listLine[1])
        print("2: ", listLine[2])
        print("3: ", listLine[3])

        # Level Unique PN

        level = 0
        levelUniquePnStruct[0] = listLine[0]

        for i in range(30):

            check = str(i) + '_'

            if(check in listLine[0]):
                levelUniquePnStruct[1] = i

        level = levelUniquePnStruct[1]
        if (level == 0) :
            mainParentPart = 1
            parentLevel = 0

        # Eng PN
        engPn = listLine[1]
        engPnList = engPn.split("_")
        engPnBase = engPnList[0]
        print("EngPnBase: ",engPnBase)
        engPnBaseList = engPnBase.split("-")
        engPnNum = engPnBaseList[1]
        print("EngPnNum: ",engPnNum)
        engPnVersion = engPnList[1]

        # ECN
        ecn = listLine[2]

        # Description
        description = listLine[3]


        #find all files for this part

        #destination filepath
        destinationFolderpath = os.path.join(outputPath,"tempProductFolders",engPn)

        #find all files with eng PN
        for root, dirs, files in os.walk(inputProductFolderName):
            for file in files:
                #if engPnBase in str(file):
                #print("file: ", file)
                #print("root: ", root)
                #print("\n")
                #    #shutil.move(path,destinationfolderpath)   
                
                # if the full base is in the file, or just the number and version, or the ecn
                if ((engPnBase in file) or (engPnNum in file and engPnVersion in file) or (ecn in file)):
                    # make base folder
                    if not os.path.exists(destinationFolderpath):
                        os.mkdir(destinationFolderpath)

                    fullFilepath = os.path.join(root,file)
                    #print("Full Filepath: ", fullFilepath)

                    if "BOM" in file:
                        destinationFolderpathSub = os.path.join(destinationFolderpath, "BOMs")
                        print("BOMFile: ", file)
                        if not os.path.exists(destinationFolderpathSub):
                            os.mkdir(destinationFolderpathSub)
                        shutil.move(fullFilepath,destinationFolderpathSub)

                    elif "ECN" in file:
                        destinationFolderpathSub = os.path.join(destinationFolderpath, "ECNs")
                        print("ECNFile: ", file)
                        if not os.path.exists(destinationFolderpathSub):
                            os.mkdir(destinationFolderpathSub)
                        shutil.move(fullFilepath,destinationFolderpathSub)

                    else:
                        destinationFolderpathSub = os.path.join(destinationFolderpath, "SPECs")
                        print("SPECFile: ", file)
                        if not os.path.exists(destinationFolderpathSub):
                            os.mkdir(destinationFolderpathSub)
                        shutil.move(fullFilepath,destinationFolderpathSub)


        #globFind = str(inputProductFolderName) + "/BOMs/" + engPnBase + "*"
        #print(globFind)
        #files = glob.glob(globFind, recursive=True)






        #for path in Path(inputProductFolderName).rglob("*"+ str(engPn) +"*"):
        #    print ("engPn Path: ", path)
        #    if not os.path.exists(destinationFolderpath):
        #        os.mkdir(destinationFolderpath)

        #    shutil.move(path,destinationFolderpath)
        #print("finished EngPn")
        #time.sleep(1)

        ## find all files with ecn
        #for path in Path(inputProductFolderName).rglob("*"+ str(engPn) +"*"):
        #    print ("ecn Path: ", path)
        #    if not os.path.exists(destinationFolderpath):
        #        os.mkdir(destinationFolderpath)

        #    shutil.move(path,destinationFolderpath)
        #print("finished ECN")
        #time.sleep(1)

        #input()
            








time.sleep(5)


        




